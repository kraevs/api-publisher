import argparse
from dataclasses import dataclass
from typing import Dict, Callable

import yaml


@dataclass
class ConfluenceSettings:
    confluence_base_api: str
    credentials: str
    space_key: str
    root_page: str


VersionKey = str


@dataclass
class ApiSpecification:
    version: VersionKey
    draft: bool


class Change:
    pass


@dataclass
class DeletePage(Change):
    api_key: str


@dataclass
class UpdatePage(Change):
    api_key: str
    spec: ApiSpecification


@dataclass
class CreatePage(Change):
    spec: ApiSpecification


def get_current_api_specs(confluence_settings: ConfluenceSettings) -> Dict[VersionKey, ApiSpecification]:
    return {}


def publish_api_to_confluence(
        branch: str,
        confluence_base_api: str,
        credentials: str,
        space_key: str,
        root_page: str,
        api: ApiSpecification,
        confluence_change_processor: Callable[[Change], None],
) -> None:
    changes = []

    confluence_settings = ConfluenceSettings(
        confluence_base_api=confluence_base_api,
        credentials=credentials,
        space_key=space_key,
        root_page=root_page,
    )
    current_api_specs = get_current_api_specs(confluence_settings)
    latest_published_version = max(list(current_api_specs.keys()))

    master_branch = branch == 'master'
    if master_branch:
        if api.version in current_api_specs and not current_api_specs[latest_published_version].draft:
            raise Exception(f"API version {api.version} already published. Bump version to publish new API")
        if api.version < latest_published_version:
            raise Exception(f'API version {api.version} is less than latest published version {latest_published_version}')

        if current_api_specs[latest_published_version].draft:
            if latest_published_version == api.version:
                changes.append(UpdatePage(api_key=latest_published_version, spec=api))
            else:
                changes.append(DeletePage(api_key=latest_published_version))
                changes.append(CreatePage(spec=api))
        else:
            changes.append(CreatePage(spec=api))
    else:
        if api.version in current_api_specs and not current_api_specs[latest_published_version].draft:
            raise Exception(f"API version {api.version} already published. Bump version to publish new API")
        if api.version < latest_published_version:
            raise Exception(f'API version {api.version} is less than latest published version {latest_published_version}')

        if current_api_specs[latest_published_version].draft:
            if latest_published_version == api.version:
                changes.append(UpdatePage(api_key=latest_published_version, spec=api))
            else:
                raise Exception(f'Two draft versions are not supported')
        else:
            changes.append(CreatePage(spec=api))

    if not changes:
        print('No changes in API')
        return

    for change in changes:
        confluence_change_processor(change)



def parse_api_spec(api_raw: dict, draft: bool) -> ApiSpecification:
    return ApiSpecification(
        draft=draft,
    )


def parse_options():
    parser = argparse.ArgumentParser(description='API publisher utility')
    parser.add_argument(dest='filename', type=str, help='OpenAPIv3 API specification file path')
    parser.add_argument('--branch', required=True, help='Git branch name. Examples: master, feature/ABC-1-new-feature')
    parser.add_argument('--confluence_url', required=True, help='Confluence base url. Example: https://confluence.company.com')
    parser.add_argument('--user_password', required=True,  help='Confluence user:password credentials')
    parser.add_argument('--root_page', required=True, help='Confluence root page for API pages')
    parser.add_argument('--space_key', required=True, help='Confluence space key')
    return parser.parse_args()


def load_api(api_filename: str) -> ApiSpecification:
    with open(api_filename) as api_file:
        return parse_api_spec(yaml.load(api_file))


def main():
    args = parse_options()

    print(f'publishing API from file {args.filename}')
    api_spec = load_api(args.filename)
    publish_api_to_confluence(
        branch=args.branch,
        confluence_base_api=args.confluence_url,
        credentials=args.user_password,
        space_key=args.space_key,
        root_page=args.root_page,
        api=api_spec,
    )


if __name__ == "__main__":
    main()
